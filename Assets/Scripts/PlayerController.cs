﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float moveSensitivity = 3F;
	public float jumpSensitivity = 300F;
	public Transform leftLeg, rightLeg;
	public GUIText loseText;

	private Animator anim;

	private float frameRate = 60F;
	private Quaternion turnLeft, turnRight;

	void Start () {
		anim = GetComponent<Animator> ();
		turnLeft = new Quaternion (0F, 180F, 0F, 0F);
		turnRight = new Quaternion (0F, 0F, 0F, 0F);
	}

	void Update () {
		// moving the player left and right
		Vector3 movement;
		float horizontal;
		horizontal = Input.GetAxis ("Horizontal");
		movement = new Vector3 (horizontal, 0.0F, 0.0F);
		transform.position = transform.position + movement * moveSensitivity * Time.deltaTime;
		if (horizontal < 0F)
						transform.rotation = turnLeft;
				else
						transform.rotation = turnRight;
		 
		if (DetectGround (leftLeg) || DetectGround (rightLeg))
						anim.SetBool ("Falling", false);
				else 
						anim.SetBool ("Falling", true);
		anim.SetFloat ("Movement Speed", horizontal);
	}

	void FixedUpdate() {
		Jump ();
	}

	void OnTriggerEnter2D(Collider2D other) {
		// collision with a falling object
		StartCoroutine ("TimeSlowing");
		anim.SetTrigger ("Death");
	}

	IEnumerator TimeSlowing() {
		while (Time.timeScale > 0.01F) {
			//print(Time.timeScale);
			Time.timeScale = Mathf.Lerp (Time.timeScale, 0F, 0.1F);
			yield return null;
		}
		Time.timeScale = 0.0F;
		loseText.text = "You lose";
	}

	void Jump() {
		if (DetectGround(leftLeg) || DetectGround(rightLeg)) {
			if (Input.GetKeyDown(KeyCode.Space)) {
				Vector3 jump = new Vector3 (0.0F, jumpSensitivity, 0.0F);
				rigidbody2D.AddForce(jump * Time.fixedDeltaTime * frameRate);
			}
		}
	}

	bool DetectGround(Transform leg) {
		bool groundDetected = false;
		Vector2 from = new Vector2 (leg.position.x, leg.position.y);
		RaycastHit2D[] hits = Physics2D.RaycastAll (from, -Vector2.up, 0.05F);
		foreach (RaycastHit2D singleHit in hits) {
			if (singleHit.collider != null && singleHit.collider.gameObject.tag == "Ground") {
				groundDetected = true;
			}
		}
		return groundDetected;
	}

	void OnDrawGizmos() {
		Vector2 from = new Vector2 (leftLeg.position.x, leftLeg.position.y);
		Vector2 to = from - Vector2.up * 0.05F;
		Gizmos.DrawLine (from, to);
	}
}
