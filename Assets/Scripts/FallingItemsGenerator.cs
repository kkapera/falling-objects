﻿using UnityEngine;
using System.Collections;

public class FallingItemsGenerator : MonoBehaviour {

	public Rigidbody2D fallingItemPrefab;
	public Camera cam;
	public float initialBirthDelay = 2.0F;

	private float birthDelay;
	private float fallingWidth;

	void Start() {
		cam = Camera.main;
		Vector3 pos = cam.ScreenToWorldPoint(new Vector3(Screen.width,0F,0F));
		fallingWidth = 2F * pos.x - fallingItemPrefab.gameObject.GetComponent<SpriteRenderer>().sprite.bounds.size.x;
		birthDelay = initialBirthDelay;
		StartCoroutine ("InstantiateItem");
	}

	IEnumerator InstantiateItem() {
		while (true) {
			Rigidbody2D itemInstance;
			Vector3 instancePosition = new Vector3(transform.position.x + fallingWidth * (Random.value - 0.5F), 
			                               transform.position.y, transform.position.z);
			itemInstance = Instantiate(fallingItemPrefab, instancePosition, transform.rotation) as Rigidbody2D;
			Destroy(itemInstance.gameObject, 5F);

			birthDelay *= 0.9F;
			yield return new WaitForSeconds (birthDelay);
		}
	}
}
